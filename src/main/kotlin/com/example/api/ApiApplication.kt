package com.example.api

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@SpringBootApplication
class ApiApplication


fun main(args: Array<String>) {
    runApplication<ApiApplication>(*args)
}

@RestController
@RequestMapping("/")
class HelloController(
    @Value("\${k8s.pod-id}") val podId: String,
    @Value("\${k8s.message}") val message: String
){

    @GetMapping
    fun get() = Response(podId, message)
}

data class Response(val podId: String, val message: String)

@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
    }
}