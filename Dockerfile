FROM amazoncorretto:17-alpine-jdk

COPY target/api-0.0.1-SNAPSHOT.jar .

CMD ["java", "-jar", "api-0.0.1-SNAPSHOT.jar"]